// SLAVE DEVICE CODE FOR WIRED MODBUS COMMUNICATION  

#include "ModbusRtu.h"

// assign the Arduino pin that must be connected to RE-DE RS485 transceiver
#define TXEN 2
#define slave_id 21
#define no_ofrooms 3
// data array for modbus network sharing
 uint16_t au16data[11] = {1,1,1,1,1,1,1,1,1,1,0};
 int i,j,state=0;
 
/**
 *  Modbus object declaration
 *  u8id : node id = 0 for master, = 1..247 for slave
 *  u8serno : serial port (use 0 for Serial)
 *  u8txenpin : 0 for RS-232 and USB-FTDI 
 *               or any pin number > 1 for RS-485
 */
  Modbus slave(slave_id,0,TXEN); // this is slave @1 and RS-485 // argument 1- this is slave id . 2- serial port number, 
                                                       // 3- pin number for RE and DE
  void setup() 
             {
              int8_t state = 0;
              slave.begin(9600);           // baud-rate at 9600
              //slave.setTimeOut(200);     // MESSAGE Tmout for bus
              //Serial.begin(9600);        // baud rate at 9600 for serial communication
              pinMode(2,OUTPUT);    
              pinMode(3,INPUT);            // INTERRUPT  pin
              
              for(j=0;j<no_ofrooms;j++)
              {
              pinMode(j+4,OUTPUT);
              digitalWrite(j+4,HIGH);
              }
            }
 
    void loop()
     {
         if(digitalRead(3)==1)
          {
           // box is open
           au16data[10]=1;
          }
         else
          {
             // box is close.
             if(au16data[10]==1)
              {
               au16data[10]=2;            
              }               
          }
        
      state=slave.poll(au16data,11);
      
      if(state>4)
      {
        if(au16data[10]==2)
             au16data[10]=0;
        for(i=0;i<no_ofrooms;i++)
            {
              digitalWrite(i+4,au16data[i]);
              
            }  
      }
    }
